# Mid-term Progress Report

- Project: Decentralized assertion platform on the blockchain
- Name: Long Isaac Kwan
- Student ID: 1155072554
- Supervisor: Prof CHAN, Lai-Wan

This report is submitted as a component of **CSCI4998 Final Year Project I**.

## Review
The project aims to put the Web of Trust (WOT) on a blockchain, which allows greater transparency and auditability. In addition, it facilitates a wider spectrum of "smart contracts" by transferring knowledge in the physical world into the Internet in a decentralized manner.

## Milestones
The following are the milestones set:

| Milestone | Target | Date |
| --------- | ------ | ---- |
| 1         | Decide on the algorithm for determining trust on a claim | 2017-10-13 |
| 2         | Design the library's interface; interacts with a dummy registry | ~~2017-10-27~~ 2017-10-31 |
| 3         | Design the data structure & optimized algorithm for the central registry | 2017-11-10 |
| 4         | Update the library to use the new registry | 2017-11-17 |
| 5         | Develop a simple application to showcase the library's capabilities | 2017-11-30 |

In particular, the first two milestones (should) have been reached. The following sections are devoted to discuss the outputs.

## Milestone 1: Deciding on the algorithm
A very brief discussion has been made available at [https://gitlab.com/isaackwan-fyp/docs/blob/master/algorithm.md](https://gitlab.com/isaackwan-fyp/docs/blob/master/algorithm.md). Ultimately it was decided to move forward with PageRank.

## Milestone 2: Library interface
### Context
Ethereum, the second-largest cryptocurrency/blockchain by market cap, is selected. It is one of the earliest "smart contract" platforms. The Ethereum Virtual Machine (EVM) accepts arbitrary bytecodes although most if not all contracts are programmed in Solidity, a statically-typed programming language.

### Functions (to be) implemented
- register()
- friend(id1, id2)
- unfriend(id1, id2)
- isFriend(id1, id2)
- pagerank(id)

### Current stage
Everything works, except pagerank() which returns 0 currently for an unknown reason.

### Problems faced
#### Solidity is extremely restrictive
Although the byte code used by Ethereum Virtual Machine (EVM) supports a large range of language constructs, the Solidity compiler does not implement them and poses huge restrictions on the algorithms. Case in point is two-dimensional arrays, which is essentially to store a matrix of friendships. It was only implemented in the compiler in the Aug 25 release, and is currently an experimental feature, hiding behind a custom flag. What makes the matter worst is that downstream libraries that execute the compiled code in the EVM and retrieve the return value has yet to support such 2D-array.

#### Debugging is very hard, if not impossible
The Solidity development experience is extremely frustrating. The reason is two-fold: (1) the compiler does not flag any potential errors and blindly convert any Solidity code into EVM byte codes and (2) when EVM throws an exception, it provides no context, only yielding an text "invalid opcode". Debugger, even a simple one, seem to be a remote goal.

## Future plans
Given that the friend registry (milestone 3 & 4) is implemented now, but PageRank computational (milestone 2) is not fully operational yet, I intend to continue working on it as well as milestone 5 for the time being.

In the second term I'd be very interested to work on either downstream application of the library or tooling (e.g. debugger) for the Ethereum VM and Solidity.


## Risks
### Restrictions by Solidity
Ethereum is so restrictive that I have considered switching to other programming languages. However, other competing blockchains also suffer from other issues. For instance, NEO, which supports C#, Java and Python (using Microsoft CLR IL), does not support simulation runs of contract that mutates the contract's state (storage).

### High computational cost
As a reward for nodes on the ledger for storing & executing the codes, "gas" must be paid for each state mutation. Currently, because of lack of optimized data structure & instructions (eg SIMD), a lot of algorithms are run unoptimized. For instance matrix additions and multiplications are done with the so-called naive multiplication algorithm. This results in impractical contracts due to extremely high gas prices. Currently this is mitigated by running a local EVM (as opposed to having the other nodes on the blockchain to compute it).